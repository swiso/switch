## Switch.css

Switch.css is a fork of Spectre.css - a lightweight, responsive and modern CSS
framework.

- Lightweight (~10KB gzipped) starting point for your projects
- Flexbox-based, responsive and mobile-friendly layout
- Elegantly designed and developed elements and components

We will focus on simplicity and on CSS-only usage. Therefore, most of the
features requiring JavaScript will be removed.

## Documentation and examples

For now, the content is the same, so find the links to the original Spectre.css
documentation below.

### Elements

- [Typography](https://picturepan2.github.io/spectre/elements/typography.html)
- [Tables](https://picturepan2.github.io/spectre/elements/tables.html)
- [Buttons](https://picturepan2.github.io/spectre/elements/buttons.html)
- [Forms](https://picturepan2.github.io/spectre/elements/forms.html)
- [Icons.css](https://picturepan2.github.io/icons.css) - CSS ONLY
- [Labels](https://picturepan2.github.io/spectre/elements/labels.html)
- [Code](https://picturepan2.github.io/spectre/elements/code.html)
- [Media](https://picturepan2.github.io/spectre/elements/media.html)

### Layout
- [Flexbox grid](https://picturepan2.github.io/spectre/layout/grid.html) 
- [Responsive](https://picturepan2.github.io/spectre/layout/responsive.html)
- [Hero](https://picturepan2.github.io/spectre/layout/hero.html)
- [Navbar](https://picturepan2.github.io/spectre/layout/navbar.html)

### Components
- [Accordions](https://picturepan2.github.io/spectre/components/accordions.html)
- [Avatars](https://picturepan2.github.io/spectre/components/avatars.html)
- [Badges](https://picturepan2.github.io/spectre/components/badges.html)
- [Breadcrumbs](https://picturepan2.github.io/spectre/components/breadcrumbs.html)
- [Bars](https://picturepan2.github.io/spectre/components/bars.html)
- [Cards](https://picturepan2.github.io/spectre/components/cards.html)
- [Chips](https://picturepan2.github.io/spectre/components/chips.html)
- [Empty states](https://picturepan2.github.io/spectre/components/empty.html)
- [Menu](https://picturepan2.github.io/spectre/components/menu.html)
- [Nav](https://picturepan2.github.io/spectre/components/nav.html)
- [Modals](https://picturepan2.github.io/spectre/components/modals.html)
- [Pagination](https://picturepan2.github.io/spectre/components/pagination.html)
- [Panels](https://picturepan2.github.io/spectre/components/panels.html)
- [Popovers](https://picturepan2.github.io/spectre/components/popovers.html)
- [Steps](https://picturepan2.github.io/spectre/components/steps.html)
- [Tabs](https://picturepan2.github.io/spectre/components/tabs.html)
- [Tiles](https://picturepan2.github.io/spectre/components/tiles.html)
- [Toasts](https://picturepan2.github.io/spectre/components/toasts.html)
- [Tooltips](https://picturepan2.github.io/spectre/components/tooltips.html)

### Utilities

- [Utilities](https://picturepan2.github.io/spectre/utilities.html) - colors, display, divider, loading, position, shapes and text utilities

### Experimentals
- [360-Degree Viewer](https://picturepan2.github.io/spectre/experimentals/viewer-360.html) - CSS ONLY
- [Autocomplete](https://picturepan2.github.io/spectre/experimentals/autocomplete.html)
- [Calendars](https://picturepan2.github.io/spectre/experimentals/calendars.html)
- [Carousels](https://picturepan2.github.io/spectre/experimentals/carousels.html) - CSS ONLY
- [Comparison Sliders](https://picturepan2.github.io/spectre/experimentals/comparison.html) - CSS ONLY
- [Filters](https://picturepan2.github.io/spectre/experimentals/filters.html) - CSS ONLY
- [Meters](https://picturepan2.github.io/spectre/experimentals/meters.html)
- [Off-canvas](https://picturepan2.github.io/spectre/experimentals/off-canvas.html) - CSS ONLY
- [Parallax](https://picturepan2.github.io/spectre/experimentals/parallax.html) - CSS ONLY
- [Progress](https://picturepan2.github.io/spectre/experimentals/progress.html)
- [Sliders](https://picturepan2.github.io/spectre/experimentals/sliders.html)
- [Timelines](https://picturepan2.github.io/spectre/experimentals/timelines.html)