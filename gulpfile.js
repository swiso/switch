const gulp = require('gulp');
const sass = require('gulp-sass');
const pug  = require('gulp-pug');
const fs   = require('fs');

const beautify = require('gulp-beautify');
const minify = require('gulp-clean-css');
const prefix = require('gulp-autoprefixer');
const rename = require('gulp-rename');

function build(done) {
  gulp.src('./src/*.scss')
    .pipe(
      sass({
        outputStyle: 'compact',
          precision: 10
      }).on('error', sass.logError))
    .pipe(prefix())
    .pipe(beautify.css({ indent_size: 2 }))
    .pipe(gulp.dest('./dist'))
    .pipe(minify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist'));
  done();
}

function docs(done) {
  gulp.src('./docs/index.pug')
    .pipe(pug())
    .pipe(beautify.html({ indent_size: 2 }))
    .pipe(gulp.dest('./dist/'));
  done();
}

exports.build = build;
exports.docs = docs;
exports.default = build;
